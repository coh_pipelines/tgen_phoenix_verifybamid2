ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_verifybamid2:1.0.6

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module
